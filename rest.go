package objectia

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"syscall"
	"time"
)

const timestampFormat = "Mon, Jan 2 2006 15:04:05 MST"

// Response model
type Response struct {
	Status  int    `json:"status"`
	Success bool   `json:"success"`
	Message string `json:"message"`
	//Data    map[string]interface{} `json:"data"`
	Data interface{} `json:"data"`
	//Code string                 `json:"code"`
}

func (c *Client) get(path string, oldEtag *ETag, result interface{}) (*ETag, error) {
	req, err := c.newRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	// Append old ETag to headers
	if oldEtag != nil {
		req.Header.Set("If-None-Match", oldEtag.Tag)
		req.Header.Set("If-Modified-Since", oldEtag.LastModified.Format(timestampFormat))
	}

	resp, err := c.execute(req, result)
	//fmt.Println("StatusCode:", resp.StatusCode)

	etag := new(ETag)
	if resp != nil {
		etag.Tag = resp.Header.Get("ETag")
		t, err := time.Parse(timestampFormat, resp.Header.Get("Last-Modified"))
		if err == nil {
			etag.LastModified = t
		}
	}
	return etag, err
}

func (c *Client) post(path string, params *Parameters, result interface{}) error {
	req, err := c.newRequest("POST", path, params)
	if err != nil {
		return err
	}

	_, err = c.execute(req, result)
	return err
}

func (c *Client) put(path string, params *Parameters, result interface{}) error {
	req, err := c.newRequest("PUT", path, params)
	if err != nil {
		return err
	}
	_, err = c.execute(req, result)
	return err
}

func (c *Client) delete(path string, result interface{}) error {
	req, err := c.newRequest("DELETE", path, nil)
	if err != nil {
		return err
	}
	_, err = c.execute(req, result)
	return err
}

func (c *Client) newRequest(method, path string, params *Parameters) (*http.Request, error) {
	var body io.ReadWriter
	var err error
	if params != nil {
		body, err = params.Encode()
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, c.apiBaseURL+path, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+c.apiKey)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", userAgent)
	if params != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	return req, nil
}

func (c *Client) execute(req *http.Request, result interface{}) (*http.Response, error) {
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, checkConnectError(err)
	}
	defer closeResponse(resp)

	if resp.StatusCode == 200 || resp.StatusCode == 201 {
		err = parseResponse(resp, result)
	} else {
		err = parseErrorResponse(resp)
	}
	return resp, err
}

func checkConnectError(err error) error {
	switch t := err.(type) {
	case *url.Error:
		if err, ok := t.Err.(net.Error); ok && err.Timeout() {
			return ErrConnectionTimedout
		}
		if opErr, ok := t.Err.(*net.OpError); ok {
			if sysErr, ok := opErr.Err.(*os.SyscallError); ok {
				if sysErr.Err == syscall.ECONNREFUSED {
					return ErrConnectionRefused
				}
			} else {
				//NOTE: Not sure if this is correct!
				return ErrUknownHost
			}
		}
		/* Pretty sure this only occur during read/write and not connect.....
		case net.Error:
		if t.Timeout() {
			return errors.New("Connection timed out")
		}*/
	}
	return err
}

func closeResponse(resp *http.Response) {
	err := resp.Body.Close()
	if err != nil {
	}
}

func parseResponse(resp *http.Response, result interface{}) error {
	if resp.Body == nil {
		return newError(resp.StatusCode, "Server returned no data")
	}
	body, _ := ioutil.ReadAll(resp.Body)
	err := json.Unmarshal(body, result)
	if err != nil {
		err = newError(resp.StatusCode, "Unexpected response from server")
	}
	return err
}

func parseErrorResponse(resp *http.Response) error {
	if resp.Body == nil {
		return newError(resp.StatusCode, "Server returned no data")
	}

	body, _ := ioutil.ReadAll(resp.Body)
	var err error
	switch resp.StatusCode {
	case 304:
		err = ErrNotModified
	case 500:
		err = newError(resp.StatusCode, "Internal server error")
	case 502:
		err = newError(resp.StatusCode, "Bad gateway")
	case 503:
		err = newError(resp.StatusCode, "Service unavailable")
	}
	if err != nil {
		return err
	}

	var result Response
	err = json.Unmarshal(body, &result)
	if err != nil {
		err = newError(resp.StatusCode, "Unexpected response from server")
	} else if len(result.Message) == 0 {
		err = newError(resp.StatusCode, "Unexpected response from server")
	} else {
		err = newError(resp.StatusCode, result.Message)
	}
	return err
}
